const timelineItems = document.querySelectorAll('#timeline li');

// Needs to be FULLY in view port
const isInViewport = el => {
  const rect = el.getBoundingClientRect();
  return (
    rect.top >= 0 &&
    rect.left >= 0 &&
    rect.bottom <=
      (window.innerHeight || document.documentElement.clientHeight) &&
    rect.right <= (window.innerWidth || document.documentElement.clientWidth)
  );
};

// Add class "show" to item
const run = () =>
  timelineItems.forEach(item => {
    if (isInViewport(item)) {
      item.classList.add('show');
    }
  });

// Events

// Window loads
window.addEventListener('load', run);
// Window resize
window.addEventListener('resize', run);
//Scroll page
window.addEventListener('scroll', run);
